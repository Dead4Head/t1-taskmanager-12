package ru.t1.amsmirnov.taskmanager.controller;

import ru.t1.amsmirnov.taskmanager.api.ICommandController;
import ru.t1.amsmirnov.taskmanager.api.ICommandService;
import ru.t1.amsmirnov.taskmanager.model.Command;
import ru.t1.amsmirnov.taskmanager.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;

        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        System.out.println("[INFO]");
        System.out.printf("Available processors (cores): %s \n", availableProcessors);
        System.out.printf("Free memory: %s \n", freeMemoryFormat);
        System.out.printf("Maximum memory: %s \n", maxMemoryValue);
        System.out.printf("Total memory: %s \n", totalMemoryFormat);
        System.out.printf("Usage memory: %s \n", usageMemoryFormat);
    }

    @Override
    public void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported!");
        System.exit(1);
    }

    @Override
    public void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported!");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Smirnov Anton");
        System.out.println("email: amsmirnov@t1-consulting.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) System.out.println(command);
    }

}
