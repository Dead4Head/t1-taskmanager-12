package ru.t1.amsmirnov.taskmanager.api;

import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

}
